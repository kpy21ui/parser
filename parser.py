# -*- coding: utf-8 -*-
import re
import os
from datetime import datetime
import sqlite3
from urllib.parse import urljoin
from grab import Grab, GrabError
from pybloom import BloomFilter
from PIL import Image
import logging


logging.basicConfig(filename='/home/mar41k/djangoenv/PARSER/'+str(datetime.now().strftime("%Y-%m-%d %H:%M"))+'.log',
                    level=logging.DEBUG)

#TEST_URL = 'http://cerkva.info/uk/news/kyiv.html?start=777'
THUMBS_SIZE = 315, 210
URL = 'http://www.cerkva.info/uk/news/patriarkh.html'
BASE_URL = 'http://cerkva.info/'
DOWNLOAD_DIR = '/home/mar41k/www/venv/cerkva/media'
DB_PATH = '/home/mar41k/www/venv/cerkva/cerkva_db.sqlite3'


def get_page(url):

    g = Grab(url)
    logging.info(g)
    #g.setup(log_dir='log/')

    if g.response.code != 200:
        logging.info("Error: Could not get page")
    else:
        logging.info("OK")


def get_page_count():
    g = Grab()
    #g.go(URL)
    # gets text "Сторінка 1 з 110"
    try:
        g.go(URL)
        if g.response.code != 200:
            logging.info("Error: Could not get page")
        else:
            logging.info("OK")
            page_count_text = g.doc.select('//p[@class="counter"]').text()
            # gets number 110
            page_count = int(page_count_text.split()[-1])
            return page_count
    except GrabError:
        logging.info("Error: Could not get page")


def date_format(string):
    """
    Function takes string in format 'Четвер, 12 лютого 2015, 16:15'
    and returns date in format '2015-02-02 16:15:00'
    """

    dict_month = {'січня':     "Jan",
                  'лютого':    "Feb",
                  'березня':   "Mar",
                  'квітня':    "Apr",
                  'травня':    "May",
                  'червня':    "Jun",
                  'липня':     "Jul",
                  'серпня':    "Aug",
                  'вересня':   "Sep",
                  'жовтня':    "Oct",
                  'листопада': "Nov",
                  'грудня':    "Dec"
                  }

    original_str_date = string
    # truncates the original string and returns '12 лютого 2015 16:15'
    mod_org_str = "".join(original_str_date.split(",")[1:])[1:]
    # gets month
    month = mod_org_str.split()[1]
    # checks if exists month in dict_month
    if month in dict_month.keys():
        # replaces ukrainian month on english month
        eng_date = mod_org_str.replace(month, dict_month[month])
        # converts string in date and time
        mod_date = datetime.strptime(eng_date, "%d %b %Y %H:%M")
        return mod_date


def parser(url):
    g = Grab()

    try:
        g.go(url)
        if g.response.code != 200:
            logging.info("Error: Could not get page")
            raise GrabError('Got non-200 response code.')
        else:
            logging.info("OK")
            # variable for posts
            posts = []
            # gets list of all posts on page
            post_list = g.doc.select('//div[@class="article_column column1 cols1"]')

            for post in post_list:
                # gets date of publish
                publish_date = post.select('.//span[@class="createdate"]').text()
                # gets header of post
                posts_titles = post.select('.//a[@class="contentpagetitle"]')
                # gets link on full post
                post_url = urljoin(URL, posts_titles.attr('href'))

                # gets short description of post
                post_body_exist = post.select('.//div[@class=""]/p').exists()
                post_description = None
                img_save_path = None
                img_src = None
                if post_body_exist:
                    # checks on existing second tag <p></p> in short description (what is empty)
                    post_body = post.select('.//div[@class=""]/p')
                    if len(post_body) > 1:
                        # if exists empty <p>, gets only text of first tag <p>
                        post_description = post_body[1].text()
                    else:
                        #if no exists empty <p>, simply gets text of tag <p>
                        post_description = post_body.text()

                    if post_body.select('.//img'):
                        # gets attribute src of image
                        img_save_path = post_body.select('.//img').attr('src')
                        img_src = urljoin(BASE_URL, img_save_path)
                    else:
                        img_save_path = None
                        img_src = None
                #elif
                else:
                    descr = post.select('.//div').text()
                    post_description = re.compile(r'\d{1,2} \w+ \d{4}, \d{1,2}:\d{2} ?(.*)$').search(descr).groups()[0]
                    img_save_path = post.select('.//div[@class]').select('.//img')[1].attr('src')
                    if str(img_save_path).startswith('/images'):
                        img_src = urljoin(BASE_URL, img_save_path)

                # checks on existing tag <img> in post
                # forms list of dictionaries
                posts.append({'title': posts_titles.text(),
                              'href': post_url,
                              'short_text': post_description,
                              'img_save_path': img_save_path,
                              'img': img_src,
                              'news_type': 'Ц',
                              'publish_date': date_format(publish_date)})
                # moves on full post

                detail_post = g.go(post_url)  # need to replace g.go(post_url) on function get_page(post_url)
                # gets all tags <p> with text of full post
                full_article = detail_post.select('//div[@class="full-article"]/p')
                # variable for full text of post
                post_full_text = []
                # for all <p> without last <p> with text "Прес-центр Київської Патріархії"
                for p in full_article[0:-1]:
                    post_full_text.append(p.text()+"\n")
                full_text = "".join(post_full_text)
                posts[-1]['full_text'] = full_text

                # gets all images in full post
                post_images_list = detail_post.select('//a[@class="sigProLink"]')
                post_images = []
                post_images_save_path = []
                # checks on existing images
                if post_images_list:
                    for img in post_images_list:
                        # gets attribute 'href' of images
                        img_s_path = img.attr('href')
                        post_images_save_path.append(img_s_path)
                        i = urljoin(BASE_URL, img_s_path)
                        post_images.append(i)
                else:
                    # if no images, we append 'None' in our dictionary
                    post_images.append("None")
                posts[-1]['post_images'] = post_images
                posts[-1]['post_images_save_path'] = post_images_save_path

            return posts, []
    except GrabError:
        logging.info("Error: Could not get page")
        return [], [url]


def download_image(url):
    src = url
    image_path = DOWNLOAD_DIR+src
    image_dir = os.path.dirname(image_path)
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)
    try:
        Grab(log_file='%s/%s' % (DOWNLOAD_DIR, src)).go(urljoin(BASE_URL, src))
        logging.info("Image %s downloaded" % src)
        return True
    except (GrabError, UnicodeEncodeError):
        logging.info("Download image failure")
        return None


def create_thumbs(file, size):
    file_name = DOWNLOAD_DIR+file
    file, ext = os.path.splitext(file_name)
    im = Image.open(file_name)
    im.thumbnail(size, Image.ANTIALIAS)
    im.save(file + "_thumbnail%s" % ext, "JPEG")


def titles_from_db():
    """
    Function connects to the db,
    selects news_titles from table 'News'
    and returns list of news titles
    """
    connect = None

    try:
        connect = sqlite3.connect('%s' % DB_PATH)
        logging.info("*** Opened db successfully")
        cursor = connect.execute("""SELECT article_link_on_original FROM article""")
        news_titles = [row[0] for row in cursor]
        return news_titles

    except sqlite3.Error as e:
        logging.info("*** Error %s: " % e.args[0])

    finally:
        if connect:
            connect.close()
            logging.info("*** db closed")


def write_posts_in_db(post):
    """
    Function takes dictionary (post)
    and writes it in db
    """
    post = post
    post_thumb = None
    if post['img_save_path']:
        file = post['img_save_path']
        path, ext = os.path.splitext(file)
        post_thumb = path + "_thumbnail%s" % ext
    connect = None
    try:
        # connects to the database. The connect() method returns a connection object
        connect = sqlite3.connect('%s' % DB_PATH)
        cursor = connect.cursor()
        logging.info("*** Opened db successfully")

        # From the connection, we get the cursor object.
        # The cursor is used to traverse the records from the result set.
        #cursor = connect.cursor()

        # executes the SQL statement

        cursor.execute("INSERT INTO article(article_title, article_full_text, article_date, article_image, "
                        "article_link_on_original, article_news_type, article_thumbnail, article_short_text)"
                        " values (?, ?, ?, ?, ?, ?, ?, ?)",
                        [post['title'], post['full_text'], post['publish_date'], post['img_save_path'],
                        post['href'], post['news_type'], post_thumb, post['short_text']])

        #connect.execute("DELETE FROM image where height=660")

        connect.commit()

        logging.info("inserted")
        last_id = int(cursor.lastrowid)

        if post['post_images_save_path']:
            for pic in post['post_images_save_path']:
                file = pic
                path, ext = os.path.splitext(file)
                post_imgs_thumb = path + "_thumbnail%s" % ext
                cursor.execute("INSERT INTO article_articleimages(article_id, article_thumb, article_img)"
                               "VALUES (?, ?, ?)", [last_id, post_imgs_thumb, pic])
                connect.commit()
                logging.info("internal image inserted")

        #logging.info("deleted rows: ", connect.total_changes)

        #cursor = connect.execute("SELECT id, image, height, width FROM image")
        #logging.info(cursor.fetchall())
        #for row in cursor:
        #    logging.info("ID = ", row[0])
        #    logging.info("Image path = ", row[1])
        #    logging.info("image height = ", row[2])
        #    logging.info("image weight = ", row[3])
        #max_id = cursor.lastrowid
        #logging.info("maxx = ", max_id)

    except sqlite3.Error as e:
        if connect:
            connect.rollback()
        logging.info("*** Error %s: " % e.args[0])

    finally:
        if connect:
            connect.close()
            logging.info("*** db closed")


def add_to_bloom_filter():
    bf = BloomFilter(10000000, 0.01)
    logging.info("Кол-во еле-тов: %d" % len(bf))
    for elem in titles_from_db():
        bf.add(elem)
    logging.info("New count of bloom %d" % len(bf))
    return bf


def task(posts):
    bloom_f = add_to_bloom_filter()
    for post in posts:
        logging.info("*"*30)
        logging.info("#"*50)
        if post['href'] not in bloom_f:

            # Saves picture of post
            if post['img_save_path'] is not None:
                if download_image(post['img_save_path']):
                    create_thumbs(post['img_save_path'], THUMBS_SIZE)
            # Saves internal images of post
            imgs = post['post_images_save_path']
            if imgs:
                for pic in imgs:
                    if download_image(pic):
                        create_thumbs(pic, THUMBS_SIZE)

            write_posts_in_db(post)
            logging.info(post)

        else:
            logging.info("False")
            return False
    return True


def initial_parser():
    posts, rejected_urls = parser(URL)
    yield posts, rejected_urls


def parser_all():
    posts = []
    rejected_urls = []
    page_index = 2*7
    #page_index = get_page_count()*7
    # define range from 7(index of second page) to page_index(without main page(-7 posts))
    for index in range(7, page_index, 7):
        logging.info("Parsing %d%%" % (index/(page_index-7)*100))
        post_url = urljoin(URL, "?start=%d" % index)
        logging.info(post_url)
        kill, rej = parser(post_url)
        #logging.info(kill)
        posts.extend(kill)
        rejected_urls.extend(rej)
        yield kill, rejected_urls


def main():
    for i, j in initial_parser():
        if task(i):
            for p, r in parser_all():
                if not task(p):
                    break
    #download_image('/images/stories/2015/04/pasha1/DSC04370.JPG')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()

